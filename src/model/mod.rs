use serde_json;
use serde_derive;



#[derive(Debug, Clone, Deserialize)]
pub struct Post {
	pub id: usize,
    pub md5: String,
    pub large_file_url: String,
    pub file_url: String,
	pub tag_string_general: String,
	pub is_flagged: bool,
	pub is_pending: bool,
	pub is_deleted: bool,
	pub tag_string_character: String,
	pub tag_string_copyright: String,
	pub tag_string_artist: String,
	pub tag_string_meta: String
}
