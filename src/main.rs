extern crate reqwest;
extern crate serde_json;
extern crate serde;
extern crate danbooru;
extern crate bincode;
#[macro_use]
extern crate log;
extern crate pretty_env_logger;


use danbooru::model::Post;
use std::io::Write;
use std::io::Read;
use std::fs::File;
use std::thread;
use std::path;
use std::fs;
use std::sync::Arc;
use std::sync::Mutex;


const DL_THREADS: usize = 8;
const IMAGE_PATH: &str = "./images/images/";
const IMAGE_POOL_PATH: &str = "./images/pool/";
const MAX_FAILS: usize = 250;
const PROGRESS_FILE: &str = "progress.bcode";
const FOLDER_SIZE: usize = 10000;





#[derive(Debug, Clone)]
struct DlManager {
	current_post: Arc<Mutex<usize>>,
	last_fail:    Arc<Mutex<usize>>,
	last_success: Arc<Mutex<usize>>,
	failed:       Arc<Mutex<usize>>,
	success:      Arc<Mutex<usize>>,
	total:        Arc<Mutex<usize>>,
}




impl DlManager {
	pub fn new(start: usize) -> Self {
		Self {
			current_post: Arc::new(Mutex::new(start)),
			last_fail:    Arc::new(Mutex::new(0)),
			last_success: Arc::new(Mutex::new(0)),
			failed:       Arc::new(Mutex::new(0)),
			success:      Arc::new(Mutex::new(0)),
			total:        Arc::new(Mutex::new(0)),
		}
	}


	pub fn get_next_id(&self) -> Option<usize> {
		// thread::sleep(std::time::Duration::new(2, 500_000_000));

		self.save_progress();

		if (*self.last_fail.lock().unwrap() - *self.last_success.lock().unwrap()) > MAX_FAILS {
			info!("it seems that we are ready");
			info!("telling thread to stop");

			return None
		}

		*self.current_post.lock().unwrap() += 1;
		Some(*self.current_post.lock().unwrap())
	}



	pub fn failed(&self, id: usize) {
		if *self.last_fail.lock().unwrap() > id {
			*self.last_fail.lock().unwrap() = id;
		}
	}


	pub fn finished(&self, id: usize) {
		if *self.last_success.lock().unwrap() > id {
			*self.last_success.lock().unwrap() = id;
		}
	}


	pub fn confirm(&self, id: usize) {
		debug!("job confirmed: {}", id);
		// *self.failed_in_a_row.lock().unwrap() += 1;
	}


	fn save_progress(&self) {
		let serial = bincode::serialize(
			&*self.current_post.lock().unwrap(),
			bincode::Bounded(512)
		).unwrap();


		File::create(PROGRESS_FILE).unwrap().write_all(&serial).unwrap();
	}
}






fn main() {
	pretty_env_logger::init().unwrap();

	let start: usize = match File::open(PROGRESS_FILE) {
		Ok(mut f) => {
			let mut buffer: Vec<u8> = Vec::new();
			f.read_to_end(&mut buffer).unwrap();

			bincode::deserialize(&buffer).unwrap()
		},
		Err(_) => 0
	};



	let dlm = DlManager::new(start);

	info!("spawning worker threads");

	let mut pool = Vec::new();

	for x in 0..DL_THREADS {
		info!("spawn thread {:2}", x);
		let threaded_dlm = dlm.clone();

		pool.push(
			thread::Builder::new()
				.name(format!("worker_{}", x))
				.spawn(|| worker_thread(threaded_dlm))
				.unwrap()
		);
	}

	info!("all threads spawned.. waiting for something");

	for thread in pool {
		thread.join().unwrap();
	}
}








fn worker_thread(dlm: DlManager) {
	loop {
		let looped_dlm = dlm.clone();
		let id = match looped_dlm.get_next_id() {
			Some(r) => r,
			None => {
				info!("i am a thread. I was told to stop");
				return;
			}
		};


		info!("working on post {:6}", id);

		let post = match danbooru::get_post(id) {
			Some(r) => {
				looped_dlm.confirm(id);
				r
			}
			None => {
				looped_dlm.failed(id);
				continue
			}
		};

		// println!("{:#?}", post);

		match download_and_save_post(post) {
			Some(_) => {
				looped_dlm.finished(id);
				debug!("post {} finished", id);
			},
			None => {
				looped_dlm.failed(id)
			}
		};

		eprintln!("{:6}", id);
	}
}



fn download_and_save_post(post: danbooru::model::Post) -> Option<()> {
	let file_url = path::Path::new(&post.file_url);

	info!("processing post {:6}", post.id.clone());

	// download image or get cached
	let image = match get_image(post.clone()) {
		Some(r) => r,
		None => return None,
	};


	let tags: Vec<&str> = post.tag_string_general.split(" ").collect();

	for tag in tags {
		// check if tag has incompatible characters
		let tag = &tag.replace("/", "_");

		let mut path = path::PathBuf::from(IMAGE_PATH);
		path.push(tag);

		if !path.exists() {
			fs::create_dir_all(path.clone()).expect(&format!("can't create directory: {:?}", path)); // create directory
		}

		path.push(file_url.file_name().unwrap());

		if path.exists() && path.is_file() {
			info!("{:6}: {:20} already exist", post.id.clone(), tag);
			continue
		}




		let mut file = File::create(path.clone()).unwrap();

		info!("{}: {}", post.id.clone(), tag);
		for byte in image.bytes() {
			file.write(&[byte.unwrap()]).unwrap();
		}
	}

	Some(())
}






fn already_there(post: danbooru::model::Post) -> Option<path::PathBuf> {
	// generate expected path
	let folder = generate_pool_number(post.clone());
	let filename = path::Path::new(&post.large_file_url)
		.file_name()
		.unwrap();

	let mut path = path::PathBuf::from(IMAGE_POOL_PATH);
	path.push(folder.to_string());
	path.push(filename);


	// check existence
	if path.exists() && path.is_file() {
		return Some(path.to_path_buf())
	}

	None
}





fn generate_pool_number(post: Post) -> usize {
	let id = post.id;

	(id - (id % FOLDER_SIZE)) / FOLDER_SIZE
}







fn get_image(post: Post) -> Option<Vec<u8>> {
	debug!("load image...");

	let mut data: Vec<u8> = Vec::new();

	let filename = path::Path::new(&post.file_url).file_name().unwrap();

	let pool_number = generate_pool_number(post.clone());

	let mut pool_dir = path::PathBuf::from(IMAGE_POOL_PATH);
	pool_dir.push(pool_number.to_string());

	let mut pool_path = pool_dir.clone();
	pool_path.push(filename);



	if pool_path.exists() {
		debug!("load cached image from {:?}", pool_path);

		let mut f = File::open(pool_path).unwrap();

		f.read_to_end(&mut data).unwrap();
	}
	else {
		debug!("download image from danbooru");
		data = match danbooru::get_image(post.large_file_url.clone()) {
			Some(r) => r,
			None => return None
		};



		if !pool_dir.exists() {
			fs::create_dir_all(pool_dir).unwrap();
		}


		let mut f = File::create(pool_path).unwrap();
		f.write_all(&data.clone()).unwrap();
	}

	Some(data)
}
