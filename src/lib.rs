extern crate reqwest;
#[macro_use]extern crate serde_derive;
extern crate serde_json;
extern crate serde;


pub mod model;


use reqwest::StatusCode;
use reqwest::Client;
use std::io::Write;
use std::io::Read;













pub fn get_post(post_id: usize) -> Option<model::Post> {
    let client = Client::new();
    // let useragent = header::UserAgent::new("Mozilla/5.0 (X11; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0");
    let uri: reqwest::Url = ("http://danbooru.donmai.us/posts/".to_string() + &post_id.to_string() + &".json".to_string()).parse().unwrap();

    // println!("{:#?}", uri);

    let mut res = match client.get(uri).send() {
		Err(e) => return None,
		Ok(ok) => ok
	};


    if res.status() != StatusCode::Ok {
        return None
    }

    match res.json::<model::Post>() {
        Err(e) => {
            return None
        }
        Ok(r) => return Some(r)
    }
}



pub fn get_image(url: String) -> Option<Vec<u8>> {
    let client = Client::new();
    let uri: reqwest::Url = (&url).parse().unwrap();


    let mut res = client.get(uri)
        .send()
        .unwrap();


    if res.status() != StatusCode::Ok {
        return None
    }


	let mut image = Vec::new();
	res.read_to_end(&mut image).unwrap();


    Some(image)
}
